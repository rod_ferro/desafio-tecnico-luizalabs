from collections import defaultdict
from json import dump
from os import listdir, path

INPUT_DIRECTORY = 'data'
INDEX_FILE = 'index.txt'

def create(input_directory=INPUT_DIRECTORY):
    """
    The type of index we're creating is an "inverted index" 
    (https://en.wikipedia.org/wiki/Inverted_index).
    """
    index = defaultdict(list)

    for file in listdir(input_directory):
        filepath = path.join(input_directory, file)
        with open(filepath) as f:
            for line in f:
                for word in line.split():
                    index[word].append(file)

    return index

def save(index, outfile=INDEX_FILE):
    """
    The index is saved in JSON format.
    """
    with open(outfile, 'w') as outfile:
        dump(index, outfile)

if __name__ == '__main__':
    save(create())