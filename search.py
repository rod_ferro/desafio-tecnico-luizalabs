import sys
from datetime import datetime
from json import load

def load_index(filename):
    with open(filename) as f:
        return load(f)

def search(index, terms):
    results = []

    for term in terms.split():
        if term in index:
            result = index[term]
            results.append(set(result))

    return set.intersection(*results)

def print_results(results, terms):
    results = sorted(results)
    num_results = len(results)

    print('Foram encontradas {} ocorrências pelo termo "{}".'.format(num_results, terms))
    if (num_results > 0):
        print('Os arquivos que possuem "{}" são:'.format(terms))
        print(*results, sep='\n')

if __name__ == '__main__':
    import index as idx

    if len(sys.argv) != 2:
        sys.exit('You should pass one string as an argument.')
    terms = sys.argv[1]
    index = load_index(idx.INDEX_FILE)
    results = search(index, terms)
    print_results(results, terms)