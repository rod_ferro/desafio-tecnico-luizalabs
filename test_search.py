from datetime import datetime
from os import mkdir
from os import path
from os import remove
import unittest

import index as idx
import search

class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        test = 'test'
        if not path.exists(test):
            mkdir(test)
        cls.index_file = path.join(test, idx.INDEX_FILE)
        idx.save(idx.create(), cls.index_file)
        cls.expected = __class__.expected

    @classmethod
    def tearDownClass(cls):
        remove(cls.index_file)

    def test_dataexists(self):
        self.assertTrue(path.exists(idx.INPUT_DIRECTORY))
        self.assertTrue(path.exists(self.index_file))

    def test_createindex(self):
        self.assertTrue(idx.create())

    def test_loadindex(self):
        self.assertTrue(search.load_index(self.index_file))

    def test_searchresults(self):
        index = search.load_index(self.index_file)
        results = search.search(index, 'walt disney')
        self.assertEqual(sorted(list(results)), self.expected)

    def test_searchspeed(self):
        index = search.load_index(self.index_file)
        a = datetime.now()
        results = search.search(index, 'walt disney')
        b = datetime.now()
        c = b - a
        search_time = 10 # microseconds = 0.01 milliseconds
        self.assertTrue(c.microseconds < search_time)

    expected = """
        a-cowboy-needs-a-horse.txt
        alice-and-the-three-bears.txt
        alice-helps-the-romance.txt
        alice-s-fishy-story.txt
        alpine-climbers.txt
        billposters.txt
        bone-trouble.txt
        bootle-beetle.txt
        canine-caddy.txt
        clown-of-the-jungle.txt
        cock-o-the-walk.txt
        cured-duck.txt
        dog-watch.txt
        donald-s-dream-voice.txt
        donald-s-snow-fight.txt
        dude-duck.txt
        el-gaucho-goofy.txt
        fall-out-fall-in.txt
        father-noah-s-ark.txt
        fathers-are-people.txt
        football-now-and-then.txt
        funny-little-bunnies.txt
        gallopin-gaucho.txt
        good-scouts.txt
        goofy-and-wilbur.txt
        great-guns.txt
        how-to-be-a-sailor.txt
        how-to-have-an-accident-at-work.txt
        jiminy-cricket-s-christmas.txt
        mickey-s-elephant.txt
        moochie-of-pop-warner-football.txt
        on-ice.txt
        pantry-pirate.txt
        perri.txt
        self-control.txt
        sky-scrappers.txt
        society-dog-show.txt
        spare-the-rod.txt
        tall-timber.txt
        the-four-musicians-of-bremen.txt
        the-hockey-champ.txt
        the-jazz-fool.txt
        the-little-house.txt
        the-mail-pilot.txt
        the-merry-dwarfs.txt
        the-nifty-nineties.txt
        the-pet-store.txt
        tommy-tucker-s-tooth.txt
        tugboat-mickey.txt
        two-gun-mickey.txt
        undiscovered-walt-disney-world.txt
        wide-open-spaces.txt
        working-for-peanuts.txt
      """.split()

if __name__ == '__main__':
    unittest.main()